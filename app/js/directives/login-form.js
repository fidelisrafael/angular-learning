(function() {
  'use strict';

  var app = angular.module('LoginFormDirective', []);

  app.controller('AuthController', ['$http', function($http) {
    this.authData = {};

    this.showForm = true;

    this.httpClient = $http;

    this.tryUserAuth = function() {

      var formData = this.authData;
      formData.provider = 'site';

      this.httpClient
        .post('http://localhost:4567/api/v1/users/auth', formData)
        .success(function(data) {
          if(data.status == 201){

          }
        })
        .error(function(data) {
          debugger;
        })
    };

  }]);


  app.directive('loginForm', function() {
    return {
      restrict: 'E',
      templateUrl: '/templates/login-form.html',
      controller: 'AuthController',
      controllerAs: 'login',
    };
  });

})();