(function() {
  'use strict';

  var app = angular.module('CompaniesListDirective', []);

  // Simple directive
  // <company-tile />
  app.directive('companyTile', function() {
    return {
      restrict: 'E', // Element
      templateUrl: '/templates/company-tile.html'
    };
  });

  app.controller('CompaniesListController', ['$http', function($http) {
    var self = this;

    this.companies = [];

    $http.get('http://localhost:4567/api/v1/cached/companies').success(function(data) {
      debugger;
      self.companies = data.companies;
    })

    this.selectedCompany = null;
    this.lastSelectedCompany = null;

    this.selectCompany = function(company) {
      this.lastSelectedCompany = this.selectedCompany;
      this.selectedCompany = company;
    }

    this.isSelected = function(company) {
      if (this.selectedCompany) {
        return this.selectedCompany.id == company.id;
      }

      return false;
    }

    this.previousSelected = function(company) {
      this.lastSelectedCompany == company;
    };
  }]);

  app.directive('companiesList', function() {
    return {
      restrict: 'E', // Element (can be A = Attribute | E = Element | C = ClassName or combined: eg: AE)
      templateUrl: '/templates/companies-list.html',
      controller: 'CompaniesListController', // can be a inline fuction here too
      controllerAs: 'cList' // alias to be used in template
    };
  });

})();