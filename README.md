## Angular Learning

Very simple application I've developed to start learning Angular.js.

### Getting Started

```bash
$ git clone git@bitbucket.org:fidelisrafael/angular-learning.git

$ cd angular-learning
$ npm install && npm install live-server -g

$ live-server # open web browser running server in port 8080 (with live reload)
```

So open `app/index.html` file and have fun, Angular.js code is very easy to understand. (I mean, I know that the HTML is quite strange, but go on, change the code, break things and I'm sure you'll quickly understand)

---

### Folders structure

```
app/
 -> bower_components/ # (application vendors files, such angular.js (not versionated))

 -> css/  # simple .css files

 -> js/
   -> directives/ # application custom elements

 -> templates/ # html files for custom elements or for ng-include

 -> node_modules/ # npm packages
```

#### That's all folks ;)