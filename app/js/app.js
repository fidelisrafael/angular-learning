(function() {
  'use strict';

  // simple bootstrap
  var app = angular.module('angularLearning', [
    'CompaniesFiltersDirective', 'CompaniesListDirective', 'LoginFormDirective'
  ]);

})();