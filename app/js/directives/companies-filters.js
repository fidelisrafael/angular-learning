(function() {
  'use strict';

  var app = angular.module('CompaniesFiltersDirective', []);

  var Controller = (function() {
    this.currentOrder = '+rank';

    this.orderOptions = [
      {
        label: 'Melhor Rank',
        order: '+rank'
      },
      {
        label: 'Pior Rank',
        order: '-rank'
      },
      {
        label: 'Nome (Asc)',
        order: '+name'
      },
      {
        label: 'Nome (Desc)',
        order: '-desc'
      }
    ]
  });

  app.directive('companiesFilters', function() {
    return {
      restrict: 'E',
      templateUrl: '/templates/companies-filters.html',
      controller: Controller,
      controllerAs: 'cFilters'
    };
  })

})();